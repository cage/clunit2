(in-package :cl-user)

(cl:defpackage :clunit
  (:use :cl)
  (:export
   ;; Standard functions and macros
   :*clunit-report-format*
   :*clunit-equality-test*
   :*test-output-stream*
   :clunit-report
   :clunit-test-report
   :deftest
   :defsuite
   :deffixture
   :undefsuite
   :undeftest
   :undeffixture
   :run-test
   :run-suite
   :run-all-suites
   :rerun-failed-tests
   :assert-fail
   :assert-expands
   :assert-condition
   :assert-false
   :assert-true
   :assert-eq
   :assert-eql
   :assert-equal
   :assert-equalp
   :assert-equality
   :assert-equality*
   :assert-finishes
   ;; Programmatic functions and macros
   :defined-test-p
   :defined-suite-p
   :get-child-tests
   :get-child-suites
   :get-parents-suites
   :get-root-suites
   :get-defined-tests
   :get-defined-suites
   :test-report-passed-p
   :test-report-name
   :test-reports))
